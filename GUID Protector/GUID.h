#pragma once

#include <windows.h>
#include <string>
#include <sstream>

// for smart pointers and other pretty stuff
#include <memory>
#include <vector>
#include <array>
#include <ctime>
#include <thread>

// For generating unique MAC ID.
#include <Iphlpapi.h>
#pragma comment(lib, "IPHLPAPI.lib")

/* cannot use namespace GUID since it's defined in windows.h */
namespace GUIDProtect
{
	extern const char* g_guid;

	extern const char* g_mac0;
	extern const char* g_mac1;
	extern const char* g_mac2;

	int*		crashProgram();

	class GUIDGenerator
	{
	public:
		static GUIDGenerator*		getInstance()
		{
			if( instance == nullptr )
				instance = new GUIDGenerator();

			return instance;
		}
		int							getResult();

		std::string					getMachineRegistryGUID();
		std::vector<std::string>	getMachineGUID();
		std::shared_ptr<BYTE>		getFileBuffer();

	public:
		bool						readCurrentExecutable();
		bool						patchCurrentExecutable( const std::string& startupArg );

	public:
		int			scanFile( const std::string& pattern );

	private:
		std::string		getExecutablePath();
		bool			isFileOpen();
		bool			isFirstRun();

	private:
		std::shared_ptr<BYTE>	getOpenExecutableContents( int& executableSize );
		HANDLE					openCurrentExecutable();

	private:
		bool		copyExecutable( const std::string& executablePath, const std::string& newPath );
		bool		renameExecutable( const std::string& executablePath, const std::string& newPath );
		bool		launchExecutable( const std::string& executablePath, const std::string& startupArg  );

	private:
		GUIDGenerator() {};

	private:
		static GUIDGenerator* instance;

	private:
		int			success						= 0;
		HANDLE		hFile						= nullptr;
		std::shared_ptr< BYTE > fileBuffer		= nullptr;
		int						fileSize		= 0;
	};
}
