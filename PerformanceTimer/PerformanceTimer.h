#pragma once

#include <string>
#include <map>
#include <chrono>
#include <mutex>
#include <vector>

class PerformanceTimer
{
public:
	static std::vector< PerformanceTimer* > getTimers();
	static PerformanceTimer* getInstance( const std::string& timerId = "localtimer" );
	static PerformanceTimer* createInstance(  const std::string& timerId );

	std::map<std::string, long long > PerformanceTimer::getTimes();
		
public:
	PerformanceTimer( const std::string& id, const std::string& timerID = "localtimer" );
	~PerformanceTimer();

public:
	const std::string& getTimerId();

private:
	std::chrono::time_point< std::chrono::steady_clock > startTime;
	std::string id;
	std::string timerId;

private:
	static std::vector< PerformanceTimer* > timers;
	std::map< std::string, long long >* times = new std::map< std::string, long long >();

	/* Static */
private:
	static void PerformanceTimer::insertElapsedTime( const std::string& timerId, const std::string & id, long long duration );
};