#include "PerformanceTimer.h"

/* Construct the global variables that this class uses */
std::vector< PerformanceTimer* > PerformanceTimer::timers  = std::vector< PerformanceTimer* >();
//std::map< std::string, long long > PerformanceTimer::times = std::map< std::string, long long >();

std::vector<PerformanceTimer*> PerformanceTimer::getTimers()
{
	return timers;
}

//************************************
// Method:    getInstance
// FullName:  Timer::PerformanceTimer::getInstance
// Access:    public static 
// Returns:   PerformanceTimer *
// Qualifier:
// Global instance that holds the timers.
//************************************
PerformanceTimer * PerformanceTimer::getInstance( const std::string& timerId )
{
	for( auto timer : timers )
	{
		if( timer->timerId == timerId )
			return timer;
	}

	auto timer = createInstance( timerId );
	return timer;
}

PerformanceTimer * PerformanceTimer::createInstance( const std::string& timerId )
{
	auto timer = new PerformanceTimer( "", timerId );

	if( !timerId.empty() )
		timers.push_back( timer );

	return timer;
}

//************************************
// Method:    PerformanceTimer
// FullName:  Timer::PerformanceTimer::PerformanceTimer
// Access:    public 
// Returns:   
// Parameter: const std::string & id
// Starts capturing the elapsed time until deconstructor is run.
//************************************
PerformanceTimer::PerformanceTimer( const std::string & id, const std::string& timerId )
{
	this->id		= id;
	this->timerId	= timerId;
	this->startTime	= std::chrono::steady_clock::now();
}

//************************************
// Method:    ~PerformanceTimer
// FullName:  Timer::PerformanceTimer::~PerformanceTimer
// Access:    public 
// Returns:   
// Qualifier:
// Save the elapsed time in an global map that holds all the captured times.
//************************************
PerformanceTimer::~PerformanceTimer()
{
	/* First calculate elapsed time as not to lose any extra cycles. */
	auto currentTime	= std::chrono::steady_clock::now();
	auto elapsedTime	= std::chrono::duration_cast< std::chrono::milliseconds >( currentTime - startTime ).count();

	/* Create single instance */
	if( !this->timerId.empty() && timers.empty() && timerId == "localtimer" )
		timers.push_back( new PerformanceTimer( this->id, "localtimer" ) );

	/* The only way this will be true is if */
	if( !this->timerId.empty() ) {
		insertElapsedTime( timerId, id, elapsedTime );
	} else {
		delete this->times;
	}
		
}

const std::string & PerformanceTimer::getTimerId()
{
	return timerId;
}

std::map<std::string, long long > PerformanceTimer::getTimes()
{
	return *getInstance( timerId )->times;
}

//************************************
// Method:    insertElapsedTime
// FullName:  Timer::PerformanceTimer::insertElapsedTime
// Access:    private static 
// Returns:   void
// Qualifier:
// Parameter: std::string & id
// Parameter: std::chrono::duration<long long
// Parameter: std::nano> & duration
// Insert or assign the duration inside the global map.
// This function is multi-thread safe.
//************************************
void PerformanceTimer::insertElapsedTime( const std::string& timerId, const std::string & id, long long duration )
{
	/* Make function thread safe using mutex */
	std::mutex mutex;

	/* Insert or assign the new time to the key */
	auto timer = PerformanceTimer::getInstance( timerId );

	/* Eliminate race condition */
	mutex.lock();
	timer->times->insert_or_assign( id, duration );
	mutex.unlock();
}
