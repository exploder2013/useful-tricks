#include "GUID.h"

namespace GUIDProtect
{
	/* these are templates for one of the ways to proect the application so you don't have to create your own */
	const char* g_guid						= "shkgdybuomknpotrpzgfzhlymgcwdeejpweet";
	const char* g_mac0						= "jhyfvetbdczpyf";
	const char* g_mac1						= "rejuweqoouysbu";
	const char* g_mac2						= "htiwuxkjcvwdgd";

	GUIDGenerator* GUIDGenerator::instance	= nullptr;

	std::string GUIDGenerator::getExecutablePath()
	{
		char path[ MAX_PATH * 4 ];

		if( GetModuleFileNameA( nullptr, path, MAX_PATH * 4 ) == false )
		{
			return std::string();
		}
		return std::string( path );
	}

	std::vector<std::string> GUIDGenerator::getMachineGUID()
	{
		std::vector<std::string> vMac;
		DWORD length			= 0;
		PIP_ADAPTER_INFO pInfo	= nullptr;

		/* Get the required length for adapters */
		GetAdaptersInfo( nullptr, &length );
		pInfo = reinterpret_cast<PIP_ADAPTER_INFO>( new char[ length ] );

		if( GetAdaptersInfo( pInfo, &length ) != ERROR_SUCCESS )
			return vMac;

		do {
			std::stringstream macAddress;

			for( int i = 0; i < MAX_ADAPTER_ADDRESS_LENGTH; i++ )
				macAddress << std::hex << (int)pInfo->Address[ i ];

			vMac.push_back( macAddress.str() );
			pInfo = pInfo->Next;
		} while( pInfo != nullptr );

		delete[] pInfo;
		return vMac;
	}

	int* crashProgram()
	{
		int* randPtr	= (int*)0x4000000;
		int* nullPtr	= randPtr - 0x3FFFF9C; /* results in 100 or 0x64 */
		double  msToCrash	= 0.0f;
		
		#ifdef _DEBUG
			printf( "Program would have crashed!\n" );
			return nullptr;
		#endif

		/* Initilize random seed */
		std::srand( (unsigned int)std::time( 0 ) );
		msToCrash = std::rand();

		/* Normalize the number between 0 and 1 (because RAND_MAX isn't == 1) */
		msToCrash /= RAND_MAX;

		auto crashWithDelay = [&]( int crashDelay )
		{
			Sleep( crashDelay );
			
			while( nullPtr > 0 )
			{
				nullPtr--;
			}

			*randPtr = *nullPtr;
		};

		/* Do a 1 minute MAX delay before crashing */
		std::thread( crashWithDelay, (int)(msToCrash * 60.0f) * 1000 ).detach();
		return randPtr;
	}

	int GUIDGenerator::getResult()
	{
		return success;
	}

	std::string GUIDGenerator::getMachineRegistryGUID()
	{
		char		buffer[MAX_PATH];
		DWORD		bufferSize = sizeof( buffer );

		HKEY		hKey;
		DWORD		status = 0;

		if( RegOpenKeyExA( HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Cryptography", 0, KEY_READ | KEY_WOW64_64KEY, &hKey ) != ERROR_SUCCESS )
			return std::string();

		if( ( status = RegQueryValueExA( hKey, "MachineGuid", nullptr, nullptr, (LPBYTE)buffer, &bufferSize ) ) != ERROR_SUCCESS )
		{
			RegCloseKey( hKey );
			return std::string();
		}
			

		return std::string( buffer, bufferSize );
	}

	std::shared_ptr<BYTE> GUIDGenerator::getOpenExecutableContents( int& executableSize )
	{
		DWORD					sizeRead = 0;
		executableSize = 0;

		if( isFileOpen() )
		{
			fileSize = GetFileSize( hFile, nullptr );
			if( fileSize == INVALID_FILE_SIZE )
				return nullptr;

			/* allocate the buffer for the file */
			fileBuffer = std::shared_ptr< BYTE >( new BYTE[ fileSize ], std::default_delete< BYTE[] >() );

			/* we failed to read the file */
			if( ReadFile( hFile, fileBuffer.get(), fileSize, &sizeRead, nullptr ) == false )
			{
				printf( "Error: %d\n", (int)GetLastError() );
				return nullptr;
			}
				

			/* set the variable to the file size */
			executableSize = fileSize;

			/* return the file contents */
			return fileBuffer;
		}

		return nullptr;
	}

	std::shared_ptr<BYTE> GUIDGenerator::getFileBuffer()
	{
		return fileBuffer;
	}

	bool GUIDGenerator::isFileOpen()
	{
		return ( hFile != nullptr && hFile != INVALID_HANDLE_VALUE );
	}

	bool GUIDGenerator::isFirstRun()
	{
		std::array< std::string, 4 > testCases = { "shkgdyb", "jhyfvet", "rejuweq", "htiwuxk" };

		/* If this is found, then the file has been run for the first time. */
		if( std::string( g_guid ).find( testCases.at( 0 ) ) != std::string::npos )
		{
			return true;
		} else {
			/* No point to test the MAC addresses */
			return false;
		}
	}

	int GUIDGenerator::scanFile( const std::string& pattern )
	{
		if( isFileOpen() )
		{
			/* get file contents inside fileBuffer */
			if( fileBuffer == nullptr )
				fileBuffer = getOpenExecutableContents( fileSize );
			
			if( fileBuffer != nullptr && fileSize != 0 )
			{
				for( int i = 0; i < fileSize; i += 1 )
				{
					bool found = true;
					for( size_t l = 0; l < pattern.length(); l++ )
					{
						
						if( fileBuffer.get()[ i + l ] != pattern[ l ] )
						{
							found = false;
							break;
						} 
					}

					if( found )
					{
						return i;
					}
						
				}
			}
		}

		return -1;
	}

	HANDLE GUIDGenerator::openCurrentExecutable()
	{
		std::string executablePath = getExecutablePath();

		if( executablePath.empty() == true )
			return INVALID_HANDLE_VALUE;

		if( !renameExecutable( executablePath, executablePath + ".bin" ) )
			return INVALID_HANDLE_VALUE;

		if( !copyExecutable( executablePath + ".bin", executablePath ) )
			return INVALID_HANDLE_VALUE;

		hFile = CreateFileA( executablePath.c_str() , GENERIC_ALL, FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr, OPEN_EXISTING, 0, nullptr );

		if( hFile == INVALID_HANDLE_VALUE )
			return INVALID_HANDLE_VALUE;

		/* Make the file as a hidden file */
		SetFileAttributesA( (executablePath + ".bin").c_str(), FILE_ATTRIBUTE_HIDDEN );


		return hFile;
	}

	bool GUIDGenerator::copyExecutable( const std::string & executablePath, const std::string & newPath )
	{
		if( !CopyFileA( executablePath.c_str(), newPath.c_str(), false ) )
			return false;

		return true;
	}

	bool GUIDGenerator::renameExecutable( const std::string& executablePath, const std::string& newPath )
	{
		if( !MoveFileA( executablePath.c_str(), newPath.c_str() ) )
		{
			/* ERROR_ALREADY_EXISTS */
			if( GetLastError() == 183 )
			{
				DeleteFileA( newPath.c_str() );

				if( !MoveFileA( executablePath.c_str(), newPath.c_str() ) )
					return false;
			}
		}
			

		return true;
	}

	bool GUIDGenerator::launchExecutable( const std::string & executablePath, const std::string& startupArg )
	{
	    // additional information
	    STARTUPINFOA si;     
	    PROCESS_INFORMATION pi;

	    // set the size of the structures
	    ZeroMemory( &si, sizeof(si) );
	    si.cb = sizeof(si);
	    ZeroMemory( &pi, sizeof(pi) );

		if( !CreateProcessA( executablePath.c_str(), (LPSTR)startupArg.c_str(), nullptr, nullptr, false, 0, nullptr, nullptr, &si, &pi ) )
			return false;

		return true;
	}

	bool GUIDGenerator::readCurrentExecutable()
	{
		if( openCurrentExecutable() == INVALID_HANDLE_VALUE )
			return false;

		if( getOpenExecutableContents( fileSize ) == nullptr )
			return false;

		return true;
	}

	bool GUIDGenerator::patchCurrentExecutable( const std::string& startupArg )
	{
		std::array< std::string, 4 > testCases = { "shkgdyb", "jhyfvet", "rejuweq", "htiwuxk" };
		std::array< int, 4 > caseLocations;

		std::string regGUID						= getMachineRegistryGUID();
		std::vector< std::string > macGUID		= getMachineGUID();

		SetFileAttributesA( (getExecutablePath() + ".bin").c_str(), FILE_ATTRIBUTE_NORMAL );
		DeleteFileA( (getExecutablePath() + ".bin").c_str() );

		if( isFirstRun() )
		{
			/* Replace the values and restart the program */
			if( openCurrentExecutable() == INVALID_HANDLE_VALUE )
			{
				crashProgram();
				return false;
			}
				

			/* Scan through the file and try to find the location */
			for( int i = 0; i < 4; i++ )
			{
				caseLocations.at( i ) = scanFile( testCases.at( i ) );

				#ifdef _DEBUG
					printf( "[%i]: [%X]\n", i, caseLocations.at( i )  );
				#endif // DEBUG
			}

			/* Check Registry MAC address */
			if( regGUID.empty() )
				crashProgram();

			/* Patch it */
			if( caseLocations.at( 0 ) != -1 )
			{
				memcpy( &fileBuffer.get()[ caseLocations.at( 0 ) ], regGUID.c_str(), 37 );
			} else {
				/* Something went wrong */
				DeleteFileA( getExecutablePath().c_str() );
				SetFileAttributesA( (getExecutablePath() + ".bin").c_str(), FILE_ATTRIBUTE_NORMAL );

				renameExecutable( getExecutablePath() + ".bin", getExecutablePath() );

				crashProgram();
			}

			/* Fill the entries with MAC addresses */
			for( size_t i = 0; i < macGUID.size(); i++ )
			{
				/* We can only add 3 addresses */
				if( i > 2 )
					break;

				/* Skip if invalid, shouldn't happen */
				if( caseLocations.at( i + 1 ) == -1 )
					continue;

				/* Patch it */
				memcpy( &fileBuffer.get()[ caseLocations.at( i + 1 ) ], macGUID.at( i ).c_str(), 14 );
			}

			CloseHandle( hFile );

			/* Create test file */
			DWORD  writtenBytes	= 0;
			HANDLE hNew			= CreateFileA( getExecutablePath().c_str(), GENERIC_ALL, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr );
			
			/* Write the new file to disk */
			WriteFile( hNew, fileBuffer.get(), fileSize, &writtenBytes, nullptr );
			CloseHandle( hNew );

			/* Relaunch the program */
			launchExecutable( getExecutablePath(), startupArg );
			
			/* Terminate */
			exit( 0 );

		} else {
			/* Just check if the values are correct */
			int matches = 0;

			if( strcmp( g_guid, regGUID.c_str() ) != 0 )
				crashProgram();
				
			#ifdef _DEBUG
					printf( "RegID: [%s] : [%s]\n", g_guid, regGUID.c_str() );
			#endif // DEBUG

			for( auto guid : macGUID )
			{
				/* If network GUID matches with saved ones, increment the matches counter */
				if( strcmp( guid.c_str(), g_mac0 ) == 0 || strcmp( guid.c_str(), g_mac1 ) == 0 || strcmp( guid.c_str(), g_mac2 ) == 0 )
					matches++;
			}

			#ifdef _DEBUG
					printf( "Matches: [%d]\n", matches );
			#endif // DEBUG

			/* Currently the tolerance for how many devices should match is 1 */
			if( matches < 1 )
				crashProgram();
		}
		
		success = 0x4201337; /* This will be the success value, int used instead of bool because of reverse engineering */
		return true;
	}

}

