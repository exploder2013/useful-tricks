# README #

Useful tricks that I might lose in the future, but since I don't want to reinvent the wheel, I just keep them here, so I can add them to my projects any time on any computer.

## Repository contains
 * GUIDProtect - protects the application so it cannot run on another computer, just the one that it was run off for the first time. (x86/x64)
 * MetaString - encrypts the strings at compile time and decrypts them at runtime. Makes reverse engineering slightly harder. (x86/x64)
 * PerformanceTimer - tracks time of how long specific process or function takes. - Buggy, needs reimplementation.